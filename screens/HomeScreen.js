import React from 'react';
import {
  FlatList,
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { WebBrowser } from 'expo';

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <View style={{
        flex: 1,
        backgroundColor: '#fff',
        marginTop: 20
      }}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>

          <View style={{
            flex: 1,
            backgroundColor: '#fff',
            marginLeft: 25,
            marginRight: 25,
          }}>
            <FlatList
              data={games}
              renderItem={({item}) =>
                <View
                  style={{
                    marginBottom: 45,
                  }}>

                  {/* IMAGE & TEXT */}
                  <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    marginBottom: 10
                  }}>

                      {/* IMAGE */}
                      <View style={{
                        flex: 0,
                        marginRight: 10
                      }}>
                        <Image
                          source={item.image}
                          style={{
                            width: 80,
                            height: 80,
                            resizeMode: 'contain',
                          }}
                        />
                      </View>

                      {/* TEXT */}
                      <View style={{flex: 2}}>
                        <Text>
                          <Text style={{fontSize: 17}}>{item.name}{'\n'}</Text>
                          <Text>{item.price}{'\n'}</Text>
                          <Text>
                            {item.rating}{'\n'}
                          </Text>
                          <Text style={{fontSize: 10}} numberOfLines={2}>
                            {item.tags.join(', ')}
                          </Text>
                        </Text>
                      </View>

                  </View>

                  {/* BUTTONS */}
                  <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                    <TouchableOpacity
                      style={{
                        flex: 1,
                        backgroundColor: '#EB5757',
                        borderRadius: 4,
                        marginRight: 10,
                        paddingTop: 10,
                        paddingBottom: 10
                      }}
                      onPress={() => WebBrowser.openBrowserAsync(item.eshop)}>
                      <Text style={{color: 'white', textAlign: 'center'}}>
                        Nintendo
                      </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      style={{
                        flex: 1,
                        backgroundColor: '#2D9CDB',
                        borderRadius: 4,
                        paddingTop: 10,
                        paddingBottom: 10
                      }}
                      onPress={() => WebBrowser.openBrowserAsync(item.steam)}>
                      <Text style={{color: 'white', textAlign: 'center'}}>
                        Steam
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              }
            />
          </View>
        </ScrollView>

      </View>
    );
  }

  _maybeRenderDevelopmentModeWarning() {
    if (__DEV__) {
      const learnMoreButton = (
        <Text onPress={this._handleLearnMorePress} style={styles.helpLinkText}>
          Learn more
        </Text>
      );

      return (
        <Text style={styles.developmentModeText}>
          Development mode is enabled, your app will be slower but you can use useful development
          tools. {learnMoreButton}
        </Text>
      );
    } else {
      return (
        <Text style={styles.developmentModeText}>
          You are not in development mode, your app will run at full speed.
        </Text>
      );
    }
  }

  _handleLearnMorePress = () => {
    WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/guides/development-mode');
  };

  _handleHelpPress = () => {
    WebBrowser.openBrowserAsync(
      'https://docs.expo.io/versions/latest/guides/up-and-running.html#can-t-see-your-changes'
    );
  };
}

const games = [
  { key : 'red-strings-club'
  , name : 'The Red Strings Club'
  , image : require('../assets/images/red-strings.png')
  , rating : 'Very Positive (2,500+ reviews)'
  , price : '$14.99'
  , eshop : 'https://www.nintendo.com/games/detail/the-red-strings-club-switch'
  , steam : 'https://store.steampowered.com/app/589780/The_Red_Strings_Club/'
  , tags : ['Cyberpunk', 'Pixel Graphics', 'Story Rich', 'Adventure']
  },
  { key : 'steam-world-2'
  , name : 'SteamWorld Dig 2'
  , image : require('../assets/images/steam-world-2.png')
  , rating : 'Overwhelming Positive (1,200+ reviews)'
  , price : '$19.99'
  , eshop : 'https://www.nintendo.com/games/detail/steamworld-dig-2-switch'
  , steam : 'https://store.steampowered.com/app/571310/SteamWorld_Dig_2/'
  , tags : ['Adventure', 'Action', 'Indie', 'Metroidvania']
  },
  { key : 'dead-cells'
  , name : 'Dead Cells'
  , image : require('../assets/images/dead-cells.png')
  , rating : 'Very Positive (22,300+ reviews)'
  , price : '$24.99'
  , eshop : 'https://www.nintendo.com/games/detail/dead-cells-switch'
  , steam : 'https://store.steampowered.com/app/588650/Dead_Cells/'
  , tags : ['Rogue-like', 'Pixel Graphics', 'Metroidvania', 'Action']
  },
  { key : 'stillness-wind'
  , name : 'The Stillness of the Wind'
  , image : require('../assets/images/stillness-wind.png')
  , rating : 'Very Positive (120+ reviews)'
  , price : '$12.99'
  , eshop : 'https://www.nintendo.com/games/detail/the-stillness-of-the-wind-switch'
  , steam : 'https://store.steampowered.com/app/828900/The_Stillness_of_the_Wind/'
  , tags : ['Indie', 'Adventure', 'Singleplayer', 'Atmospheric']
  },
  { key : 'okami'
  , name : 'OKAMI HD'
  , image : require('../assets/images/okami.png')
  , rating : 'Very Positive (2,400+ reviews)'
  , price : '$19.99'
  , eshop : 'https://www.nintendo.com/games/detail/okami-hd-switch'
  , steam : 'https://store.steampowered.com/app/587620/OKAMI_HD/'
  , tags : ['Adventure', 'Great Soundtrack', 'Mythology', 'Classic']
  },
  { key : 'west-loathing'
  , name : 'West of Loathing'
  , image : require('../assets/images/west-loathing.png')
  , rating : 'Overwhelming Positive (2,800+ reviews)'
  , price : '$11.00'
  , eshop : 'https://www.nintendo.com/games/detail/west-of-loathing-switch'
  , steam : 'https://store.steampowered.com/app/597220/West_of_Loathing/'
  , tags : ['RPG', 'Comedy', 'Adventure', 'Funny']
  },
  { key : 'shovel-knight-treasure-trove'
  , name : 'Shovel Knight: Treasure Trove'
  , image : require('../assets/images/shovel-knight-treasure-trove.png')
  , rating : 'Overwhelming Positive (7,800+ reviews)'
  , price : '$24.99'
  , eshop : 'https://www.nintendo.com/games/detail/shovel-knight-treasure-trove-switch'
  , steam : 'https://store.steampowered.com/app/250760/Shovel_Knight_Treasure_Trove/'
  , tags : ['Platformer', 'Pixel Graphics', 'Great Soundtrack', '2D']
  },
];

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  listContainer: {
    flex: 1,
    backgroundColor: '#fff',
    marginLeft: 20,
  },
  listItemContainer: {
    flex: 1,
    backgroundColor: '#fff',
    marginLeft: 20,
    flexDirection:'row',
    flexWrap:'wrap',
  },
  itemImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
