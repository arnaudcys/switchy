import React from 'react';
import {
  Linking,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';

export default class SettingsScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  render() {
    return (
      <View style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
      }}>
          <View style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
          }}>
              <Text style={{fontSize: 17}}>To sponsor this app, please email</Text>
              <TouchableOpacity
                onPress={() => Linking.openURL('mailto:contact@switchy.app?subject=Sponsor')}>
                <Text style={{fontSize: 17, color: '#2e78b7'}}>
                  contact@switchy.app
                </Text>
              </TouchableOpacity>
          </View>
      </View>
    );
  }
}
